"""
Early sample scraper specifically for Hunter College 
courses. This will visit the class registration search
page for CUNY, select Hunter College and Fall 2016. 
Each subject will then have all of its classes' details 
saved to individual .csv files.

Author: adass
"""
import time
import csv

import re
import datetime

import os
import random

import ast

from selenium import webdriver
from lxml import html

from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select

from tqdm import *


search_URL = "https://hrsa.cunyfirst.cuny.edu/psc/cnyhcprd/GUEST/HRMS/c/COMMUNITY_ACCESS.CLASS_SEARCH.GBL"
institution_name = "Hunter College"
term_name = "2016 Fall Term"
term_year = "2016"
term_season = "Fall"

subject_list = ["ACC", "ACSK", "ADSUP", "AFPRL", "ANTH", "ANTHC", "ANTHP", "ARB", "ARTCR", "ARTH", "ARTLA", "ASIAN", "ASTRO", "BIOCH", "BIOL", "CEDC", "CEDCF", "CEDF", "CHEM", "CHIN", "CHND", "CLA", "COCO", "COMPL", "COMSC", "COUNM", "COUNR", "COUNS", "CSCI", "CUNBA", "DAN", "DANED", "ECC", "ECF", "ECO", "EDABA", "EDESL", "EDLIT", "EDPS", "ENRT", "BILED", "HED", "SPEDE", "FILM", "FILMP", "FREN", "FYS", "GEOG", "GEOL", "GERMN", "GRK", "GSR", "GTECH", "PGEOG", "HEBR", "HIST", "HONS", "HR", "HUM", "ILBAC", "IMA", "ITAL", "JPN", "JS", "LACS", "LAT", "LATED", "LIBR", "LING", "MAM", "MATH", "MEDIA", "MEDP", "MHC", "MLS", "MUS", "MUSED", "MUSHL", "MUSIN", "MUSPF", "MUSTH", "NURS", "ONFIL", "PERM", "PHILO", "PHYS", "POL", "POLSC", "PORT", "PSYCH", "PT", "PUPOL", "BIOS", "COMHE", "EOHS", "EPI", "HPM", "NFS", "NUTR", "PH", "QSTA", "QSTAB", "QSTB", "RAS", "REL", "RUSS", "SCI", "SEDC", "SEDF", "SOC", "SOSCI", "SPAN", "SPED", "SSW", "STABD", "STAT", "THC", "THEA", "UKR", "URBG", "URBP", "URBS", "WGS", "WGSA", "WGSI", "WGSL", "WGSP", "WGSS"]

short_wait = 2
long_wait = 10

max_credits = str(10) # Page/Driver probably expects strings (which the server then presumably turns into a number...)


def search_catalog(subject):
    """
    Use Selenium WebDriver to conduct a search for a given subject, and return the source code of the resulting search page.
    """
    driver = webdriver.Chrome()
    
    driver.get(search_URL) # 1) Get the page
    time.sleep(short_wait) # 2) Wait for it to load
    Select(driver.find_element_by_id("CLASS_SRCH_WRK2_INSTITUTION$42$")).select_by_visible_text(institution_name) # 3) Select institution
    time.sleep(short_wait) # 4) Wait for terms to load
    Select(driver.find_element_by_id("CLASS_SRCH_WRK2_STRM$45$")).select_by_visible_text(term_name) # 5) Select term
    time.sleep(short_wait) # 6) Wait for subjects to load
    Select(driver.find_element_by_id("SSR_CLSRCH_WRK_SUBJECT_SRCH$0")).select_by_value(subject) # 7) Select subject
    driver.find_element_by_id("SSR_CLSRCH_WRK_SSR_OPEN_ONLY_LBL$5").click() # 8) Show non-open classes as well
    driver.find_element_by_id("SSR_CLSRCH_WRK_UNITS_MAXIMUM$12").clear() # 9) Clear max-units input (to select it?)
    driver.find_element_by_id("SSR_CLSRCH_WRK_UNITS_MAXIMUM$12").send_keys(max_credits) # 10) Enter max-units to get pretty much all classes in results
    driver.find_element_by_id("CLASS_SRCH_WRK2_SSR_PB_CLASS_SRCH").click() # 11) Conduct search with these options
    time.sleep(long_wait) # 12) Wait for server to process and display results (also, give enough time for next automated search)
    
    result_source = driver.page_source # Obtain source code of resulting page
       
    driver.quit()
    
    return result_source

# end search_catalog()

def search_ENGL(subject, career):
    """
    Like search_catalog(), but specifically for ENGL classes, because ENGL typically has 350+ results, which CUNYFirst refuses to display without further refinement.
    This refines based on the 'Course Career' field (Undergraduate vs Graduate)... "GRAD" vs "UGRD"
    """
    
    driver = webdriver.Chrome()
    
    driver.get(search_URL) # 1) Get the page
    time.sleep(short_wait) # 2) Wait for it to load
    Select(driver.find_element_by_id("CLASS_SRCH_WRK2_INSTITUTION$42$")).select_by_visible_text(institution_name) # 3) Select institution
    time.sleep(short_wait) # 4) Wait for terms to load
    Select(driver.find_element_by_id("CLASS_SRCH_WRK2_STRM$45$")).select_by_visible_text(term_name) # 5) Select term
    time.sleep(short_wait) # 6) Wait for subjects to load
    Select(driver.find_element_by_id("SSR_CLSRCH_WRK_SUBJECT_SRCH$0")).select_by_value(subject) # 7) Select subject
    driver.find_element_by_id("SSR_CLSRCH_WRK_SSR_OPEN_ONLY_LBL$5").click() # 8) Show non-open classes as well
    
    Select(driver.find_element_by_id("SSR_CLSRCH_WRK_ACAD_CAREER$2")).select_by_value(career) # 9) NEW! For ENGL only- career option...
    
    driver.find_element_by_id("CLASS_SRCH_WRK2_SSR_PB_CLASS_SRCH").click() # 10) Conduct search with these options
    time.sleep(long_wait) # 11) Wait for server to process and display results (also, give enough time for next automated search)
    
    result_source = driver.page_source # Obtain source code of resulting page
       
    driver.quit()
    
    return result_source
    
# end search_ENGL()

def read_classes(result_page, subject):
    """
    Use lxml to read different classes by separating the search results.
    
    Note- 'Class' denotes a specific section/instance of a course that's offered by a department
    """
    class_count = int(result_page.xpath('count(//*[starts-with(@id,"MTG_CLASS_NBR$")])'))
    print '\n\n**', subject, '-', class_count, '**'
    
    current_datetime = time.strftime("[%Y-%m-%d][%H-%M-%S]")
    
    for class_num in tqdm(range(0, class_count), desc='Class'): # Using tqdm progress bar...
        # Get information for each relevant field
        
        c_num = str(class_num)
        
        course_title = result_page.xpath('//*[@id="MTG_CLASS_NBR$' + c_num + '"]/preceding::div[starts-with(@id,"win0divGPSSR_CLSRSLT_WRK_GROUPBOX2$")]/text()')[-1] # Returns unclean text? Must strip() when concatenating...?
        
        course_title = course_title.strip()
        
        class_code = result_page.xpath('//*[@id="MTG_CLASS_NBR$' + c_num + '"]/text()')
        class_section = result_page.xpath('//*[@id="MTG_CLASSNAME$' + c_num + '"]/text()')
        class_times = result_page.xpath('//*[@id="MTG_DAYTIME$' + c_num + '"]/text()')
        class_rooms = result_page.xpath('//*[@id="MTG_ROOM$' + c_num + '"]/text()')
        class_instructors = result_page.xpath('//*[@id="MTG_INSTR$' + c_num + '"]/text()')
        class_dates = result_page.xpath('//*[@id="MTG_TOPIC$' + c_num + '"]/text()')
        class_status = result_page.xpath('//*[@class="SSSIMAGECENTER"]/@alt')[class_num]
        
        # Package each field of an individual class into a dictionary
        
        class_data = {
            "course": course_title,
            "code": class_code,
            "section": class_section,
            "times": class_times,
            "rooms": class_rooms,
            "instructors": class_instructors,
            "dates": class_dates,
            "status": class_status
            }
        
        #print_classes(class_data, class_num) # For debugging, I suppose...
        
        cleaned_class_data = clean_classes(class_data)
        save_file_name = "data/" + subject + current_datetime + ".csv" # "_classes.csv"
        
        if not os.path.exists(os.path.dirname(save_file_name)):
            os.makedirs(os.path.dirname(save_file_name))
        
        with open(save_file_name, 'ab') as csvfile: # 'a' for appending...
            fieldnames = ['course_subject', 'course_number', 'course_title', 'class_code', 'section_type', 'session', 'days_times', 'class_weekdays', 'class_time_starts', 'class_time_ends', 'locations', 'instructors', 'date_starts', 'date_ends', 'status', 'institution', 'year', 'term', 'last_checked']
            
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            if class_num == 0:
                writer.writeheader() # This should only write the header if loop is in the first index!

            writer.writerow(cleaned_class_data)
           
# end read_classes()

def clean_classes(class_data):
    """
    Clean class data for better storage.
    """
    cleaned_class_data = {}
    
    course = class_data["course"]
    course = course.replace("-","")
    course_elements = course.split()
    cleaned_class_data["course_subject"]  = course_elements[0]
    cleaned_class_data["course_number"] = course_elements[1]
    course_full_title = ' '.join(course_elements[2:])
    cleaned_class_data["course_title"] = course_full_title
    
    cleaned_class_data["class_code"] = class_data["code"][0]
    
    section = class_data["section"]
    cleaned_class_data["section_type"] = section[0]
    section[1] = section[1].strip() # Strip extraneous characters from second element of section list (session information)...
    cleaned_class_data["session"] = section[1]
    
    cleaned_class_data["days_times"] = class_data["times"] # The original string denoting days and times of meetings... (as backup, in case multiple time blocks)...
    cleaned_class_data["days_times"] = ", ".join(cleaned_class_data["days_times"]) # Second step, turns the list into a "clean" string
    
    
    if "TBA" in class_data["times"][0]:
        cleaned_class_data["class_weekdays"] = "TBA"
        cleaned_class_data["class_time_starts"] = "00:00:00"
        cleaned_class_data["class_time_ends"] = "00:00:00"
    else:
        days_times_string = class_data["times"][0]
        days_times_string = days_times_string.replace("-","")
        days_times_list = days_times_string.split()
        weekdays = re.findall('[A-Z][^A-Z]*', days_times_list[0]) # Splits string at upper-case letters to separate the days (found RegEx solution on StackOverflow)...
        # These days are currently written as "Mo Tu We Th Fr Sa Su TBA (or T B A?)"
        time_starts = datetime.datetime.strptime(days_times_list[1], '%I:%M%p').time()
        time_ends = datetime.datetime.strptime(days_times_list[2], '%I:%M%p').time()
        cleaned_class_data["class_weekdays"] = weekdays # This is a list
        cleaned_class_data["class_weekdays"] = ", ".join(cleaned_class_data["class_weekdays"])
        cleaned_class_data["class_time_starts"] = time_starts
        cleaned_class_data["class_time_ends"] = time_ends
    
    cleaned_class_data["locations"] = class_data["rooms"] # This returns a list-y string, or a list that is presented as string in Django (like days_times, class_weekdays, and instructors below), so "cleaning" in next line...
    cleaned_class_data["locations"] = ", ".join(cleaned_class_data["locations"])
    cleaned_class_data["instructors"] = class_data["instructors"]
    # NEW, for cleaner!
    for elem in range(len(cleaned_class_data["instructors"])):
        cleaned_class_data["instructors"][elem] = re.sub('\, $', '', cleaned_class_data["instructors"][elem]) # This cleans out ", " from WITHIN strings... unnecessary!
    cleaned_class_data["instructors"] = ", ".join(cleaned_class_data["instructors"]) # ", " joins list elements for clean string outputs!
    
    if "TBA" in class_data["dates"][0]:
        cleaned_class_data["date_starts"] = "1900-01-01"
        cleaned_class_data["date_ends"] = "1900-01-01"
    else:
        dates_string = class_data["dates"][0]
        dates_string = dates_string.replace("-","")
        dates_list = dates_string.split()
        date_starts = datetime.datetime.strptime(dates_list[0], '%m/%d/%Y').date()
        date_ends = datetime.datetime.strptime(dates_list[1], '%m/%d/%Y').date()
        cleaned_class_data["date_starts"] = date_starts
        cleaned_class_data["date_ends"] = date_ends
        
    cleaned_class_data["status"] = class_data["status"]
    
    cleaned_class_data["institution"] = institution_name
    cleaned_class_data["year"] = term_year
    cleaned_class_data["term"] = term_season    
    
    cleaned_class_data["last_checked"] = time.strftime("%Y-%m-%d %H:%M:%S")
    
    return cleaned_class_data

#end clean_classes()


def print_classes(class_data, class_num):
    """
    Print classes to the [terminal], for debugging purposes (?) - since primary goal is to save data as CSV to be loaded into Django...
    """
    print '\n'
    print '##################################################'
    print '\n'

    print "Class: ", str(class_num)
    print "Course Title: ", class_data["course"]
    print "Code: ",
    for item in range(0, len(class_data["code"])):
        print class_data["code"][item],
    print "\nSection: ", 
    for item in range(0, len(class_data["section"])):
        print class_data["section"][item],
    print "\nTime: ", 
    for item in range(0, len(class_data["times"])):
        print class_data["times"][item],
    print "\nRoom: ",
    for item in range(0, len(class_data["rooms"])):
        print class_data["rooms"][item],
    print "\nInstructor: ",
    for item in range(0, len(class_data["instructors"])):
        print class_data["instructors"][item],
    print "\nDates: ",
    for item in range(0, len(class_data["dates"])):
        print class_data["dates"][item],
    print "\nStatus: ", class_data["status"]    

# end print_classes()
    
def main():    
    
    subject_search_list = subject_list
    
    print "\n=============================="
    print "Hunter College Classes Scraper"
    print "==============================\n"
    #Loop through all subjects in subject_search_list
    for subject in tqdm(subject_search_list, desc='Sbjct', total=len(subject_search_list), leave=True):
        
        result_source = search_catalog(subject) # Get source code of the search result page for the given subject using "Selenium WebDriver"
        result_page = html.fromstring(result_source) # Turn that source code into page (tree) to manipulate using "lxml"
        read_classes(result_page, subject) # Read the search results to get information for individual classes
    
    # Now, do ENGL... # SKIPPING DOCT, since assuming no doctoral classes?
    career_list = ["UGRD", "GRAD"]
    for career in tqdm(career_list, desc='En-UG', total=2, leave=True):
        
        result_source = search_ENGL("ENGL", career)
        result_page = html.fromstring(result_source)
        read_classes(result_page, "ENGL")

# end main()

if __name__ == "__main__":
    main()