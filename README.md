README.TXT for

/*
File: hunter_scraper.py
Name: adass
Side-Project
*/


Basic early sample scraper specifically for Hunter College courses. This will visit the class registration search page for CUNY, select Hunter College and Fall 2016. Each subject will then have all of its classes' details saved to individual .csv files. This sample project can be customized by modifying the variables in the beginning of the code.

This script depends on Selenium WebDriver, lxml, and tqdm (tqdm is just for nice progress bars in the terminal and can be removed by modifying the "for loops" that use it). The Chrome version of WebDriver is used but can be switched.

After modifying the variables (if you want), just run the script in a terminal. It should start opening pages and saving data to files in a folder called "data". The full (default) run of all subjects might take a while...